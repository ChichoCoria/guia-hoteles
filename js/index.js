$(function () {
    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover()
    $('.carousel').carousel({
        interval: 2000
      })

    /* Ejemeplo utilizando los eventos del modal, los eventos usan los verbos en infinitivo y participio para indicar cuando es el comienzo
    de una ejecucion de la accion y cuando termino la ejecucion de la accion, ej: show, shown /mostrando, mostro */
        $('#contacto').on('show.bs.modal', function (e) {
        console.log ('el modal contacto se esta mostrando');
             /* Tambien podemos cambiar las propiedades de los botones, en este ejemplo, vamos a remover la clase btn-primary y vamos a cambiarlo
          por la clase btn-outline-success, tambien con la propiedad .prop disabled, podemos desibilitar el boton.*/
          $('#contactobtn').removeClass('btn-primary');
          $('#contactobtn').addClass('btn-outline-success');
          $('#contactobtn').prop('disabled', true);
        });
         

        $('#contacto').on('shown.bs.modal', function (e) {
        console.log ('el modal contacto se mostro');
        });

        $('#contacto').on('hide.bs.modal', function (e) {
        console.log ('el modal contacto se oculta');
        /* Cuando se oculte vamos a dejar el boton con la clase btn-primary para dejarlo como estaba, y habilitamos el boton*/
          $('#contactobtn').removeClass('btn-outline-succes');
          $('#contactobtn').addClass('btn-primary');
          $('#contactobtn').prop('disabled', false);
        });

        $('#contacto').on('hidden.bs.modal', function (e) {
        console.log ('el modal contacto se oculto');
        });

    });